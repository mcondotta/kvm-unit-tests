#!/usr/bin/env python3

import sys
import glob
import os
from avocado.core.job import Job
from avocado.core.suite import TestSuite
from avocado.utils import build, cpu

# Configure and build the test binaries
cwd = os.getcwd()
build.configure(cwd, configure=None)
nproc = os.cpu_count()
build.make(cwd, extra_args="-j{} standalone".format(nproc))

testdir = os.path.join(cwd, 'tests')
os.chdir(testdir)

# Get the test binaries from the /tests/ directory
tests = glob.glob("./*")
test_names = [os.path.basename(x) for x in tests]

class_name = cpu.get_arch() + '-' + os.getenv('ACCEL', 'kvm') + '-' + os.getenv('MACHINE', 'pc')

resultxml = os.path.join(cwd, 'results.xml')
resultzip = os.path.join(cwd, class_name + '_logs.zip')

config = {
    'job.run.result.xunit.job_name': 'KVM Unit Tests',
    'job.run.result.xunit.class_name': class_name,
    'job.run.result.xunit.test_names': test_names,
    'job.run.result.xunit.output': resultxml,
    'run.test_runner': 'nrunner',
    'runner.exectest.exitcodes.skip': [2, 77],
    'run.references': tests,
    'run.results.archive': True,
    'run.results.archive_path': resultzip,
    }

suite = TestSuite.from_config(config)
with Job(config, [suite]) as j:
    sys.exit(j.run())
